package com.example.SebUzduotis;

import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigInteger;
import java.util.*;
@Service
public class IbanService {

    private static final String DEFSTRS = ""
            + "AL28 AD24 AT20 AZ28 BE16 BH22 BA20 BR29 BG22 "
            + "HR21 CY28 CZ24 DK18 DO28 EE20 FO18 FI18 FR27 GE22 DE22 GI23 "
            + "GL18 GT28 HU28 IS26 IE22 IL23 IT27 KZ20 KW30 LV21 LB28 LI21 "
            + "LT20 LU20 MK19 MT31 MR27 MU30 MC27 MD24 ME22 NL18 NO15 PK24 "
            + "PS29 PL28 PT25 RO24 SM27 SA24 RS22 SK24 SI19 ES24 SE24 CH21 "
            + "TN24 TR26 AE23 GB22 VG24 GR27 CR21";
    private static final Map<String, Integer> DEFINITIONS = new HashMap<>();

    static {
        for (String definition : DEFSTRS.split(" "))
            DEFINITIONS.put(definition.substring(0, 2), Integer.parseInt(definition.substring(2)));
    }

    void Save() {
        FileManager fileManager = new FileManager();
        try {
            Scanner myObj = new Scanner(System.in);
            String fileName;
            System.out.println("Enter IBANS file path. Example: C:\\Users\\Rytis\\Desktop\\demo\\ibans.txt");
            fileName = myObj.nextLine();
            fileManager.getIBANFromFile(fileName);
            try (PrintStream out = new PrintStream(new FileOutputStream(fileName+".out"))) {
                for (String iban : fileManager.getIBANFromFile(fileName))
                    out.printf("%s is %s.%n", iban.toUpperCase(Locale.ROOT), validateIBAN(iban) ? "Valid" : "Not Valid");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void Check() {
        Scanner myObj = new Scanner(System.in);
        String iban;
        System.out.println("Enter your IBAN");
        iban = myObj.nextLine();
        System.out.printf("%s is %s.%n", iban, validateIBAN(iban) ? "Valid" : "Not Valid");
    }

    private static boolean validateIBAN(String iban) {
        iban = iban.toUpperCase(Locale.ROOT);
        int len = iban.length();
        if (len < 4 || !iban.matches("[0-9A-Z]+") || DEFINITIONS.getOrDefault(iban.substring(0, 2), 0) != len)
            return false;
        iban = iban.substring(4) + iban.substring(0, 4);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < len; i++)
            sb.append(Character.digit(iban.charAt(i), 36));
        BigInteger bigInt = new BigInteger(sb.toString());
        return bigInt.mod(BigInteger.valueOf(97)).intValue() == 1;
    }

    List<IBAN> validateListOfIBANS(List<IBAN> ibans) {
        List<IBAN> validatedIBANS = new ArrayList<>();
        for (IBAN iban : ibans) {
            if (validateIBAN(iban.getIban())) {
                iban.setValid(true);
                validatedIBANS.add(iban);
            } else {
                iban.setValid(false);
                validatedIBANS.add(iban);
            }
        }
        return validatedIBANS;
    }
}
