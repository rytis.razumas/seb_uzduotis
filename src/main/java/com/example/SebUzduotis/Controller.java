package com.example.SebUzduotis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class Controller {

    private final IbanService ibanService;

    @Autowired
    public Controller(IbanService ibanService) {
        this.ibanService = ibanService;
    }

    @RequestMapping(value = "/validateIBAN", method = RequestMethod.POST)
    public List<IBAN> validateIBAN(@RequestBody List<IBAN> ibans) {
        return ibanService.validateListOfIBANS(ibans);
    }
}
