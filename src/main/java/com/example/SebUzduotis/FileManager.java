package com.example.SebUzduotis;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

class FileManager {

    String[] getIBANFromFile(String filePath) throws IOException {
        ArrayList<String> ibansList = new ArrayList<String>() ;
        String currentIban;
        FileReader fileLocation = new FileReader(filePath);
        int i = 0;
        try {
            BufferedReader file = new BufferedReader(fileLocation);
            while ((currentIban = file.readLine()) != null) {
                ibansList.add(currentIban);
                i++;
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ibansList.toArray(new String[]{});
    }
}
