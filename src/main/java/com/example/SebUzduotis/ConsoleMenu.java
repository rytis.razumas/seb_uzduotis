package com.example.SebUzduotis;
import java.util.Scanner;


class ConsoleMenu {

    private IbanService ibanService = new IbanService();
    private boolean exit;

    void runMenu() {
        printHeader();
        while (!exit) {
            printMenu();
            int choice = getMenuChoice();
            performAction(choice);
        }
    }

    private void printHeader() {
        System.out.println("+-----------------------------------+");
        System.out.println("|        IBAN Checker               |");
        System.out.println("+-----------------------------------+");
    }

    private void printMenu() {
        System.out.println("Please make a selection");
        System.out.println("1) Check your IBAN");
        System.out.println("2) Check your IBANS from file and save");
        System.out.println("0) Exit");
    }

    private int getMenuChoice() {
        Scanner keyboard = new Scanner(System.in);
        int choice = -1;
        do {
            System.out.print("Enter your choice: ");
            try {
                choice = Integer.parseInt(keyboard.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Invalid selection. Numbers only please.");
            }
            if (choice < 0 || choice > 4) {
                System.out.println("Choice outside of range. Please chose again.");
            }
        }
        while (choice < 0 || choice > 4);
        return choice;
    }

    private void performAction(int choice) {
        switch (choice) {
            case 0:
                System.out.println("Thank you for using our application.");
                System.exit(0);
                break;
            case 1:
                ibanService.Check();
                break;
            case 2:
                ibanService.Save();
                System.out.println("Iban saved");
                break;
            default:
                System.out.println("Unknown error has occured.");
        }
    }
}
